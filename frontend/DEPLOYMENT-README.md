# Deployment
- Frontend has been built for production (`ng build --prod`) and is hosted in S3, with a cloudfront distribution on top.
- The cloudfront distribution is deployed at 'wipeup.wipedown.me'
- Does not use SSL/TLS though we should definitely set this up before a proper production deployment
