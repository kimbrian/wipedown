import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Observable} from 'rxjs';

export interface Option {
  value: any;
  display: string;
}

export interface EditableField {
  fieldName: string;
  displayName: string;
  fieldType: 'select'|'text'|'boolean';
  optionsObservable?: Observable<Option[]>;
  options?: Option[];
  value: any;
}

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditableField[]) {}

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {
    for (const editableField of this.data) {
      if (editableField.fieldType === 'select') {
        console.log(editableField);
        editableField.optionsObservable.subscribe(
          o => editableField.options = o
        );
      }
    }
  }

}
