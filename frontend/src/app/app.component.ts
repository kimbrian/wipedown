import { Component } from '@angular/core';
import {CustomIconService} from './custom-icon.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  constructor(
    private customIconService: CustomIconService
  ) {
    this.customIconService.init();
  }
}
