export interface IComment {
  id: number;
  content: string;
  createdAt: Date;
  commentType: string;
  author: number;
  bathroom: number;
}
