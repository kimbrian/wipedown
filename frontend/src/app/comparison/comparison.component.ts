import {Component, OnInit} from '@angular/core';
import {BathroomService} from '../bathroom.service';
import {IBathroom} from '../bathroom';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-comparison',
  templateUrl: './comparison.component.html',
  styleUrls: ['./comparison.component.scss']
})
export class ComparisonComponent implements OnInit {
  public bathroomLeft: IBathroom;
  public bathroomRight: IBathroom;
  public smellWinner: number = null;
  public cleanlinessWinner: number = null;
  public viewWinner: number = null;
  public sizeWinner: number = null;
  public accessibilityWinner: number = null;

  constructor(private bathroomService: BathroomService, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.smellWinner = null;
    this.viewWinner = null;
    this.cleanlinessWinner = null;
    this.sizeWinner = null;
    this.accessibilityWinner = null;
    this.bathroomService.get2RandomBathrooms().subscribe(bathrooms => {
      this.bathroomLeft = bathrooms[0];
      this.bathroomRight = bathrooms[1];
    });
  }

  getDisplayName(attr: 'smellWinner' | 'cleanlinessWinner' | 'viewWinner' | 'sizeWinner' | 'accessibilityWinner') {
    return {
      smellWinner: 'Smell',
      cleanlinessWinner: 'Cleanliness',
      viewWinner: 'View',
      sizeWinner: 'Size',
      accessibilityWinner: 'Accessibility'
    }[attr];
  }

  getClass(bathroom: IBathroom, attr: 'smellWinner' | 'cleanlinessWinner' | 'viewWinner' | 'sizeWinner' | 'accessibilityWinner') {
    return this[attr] === bathroom.id ? 'winner' : this[attr] === null ? '' : 'loser';
  }

  setWinner(bathroom: IBathroom, attr: 'smellWinner' | 'cleanlinessWinner' | 'viewWinner' | 'sizeWinner' | 'accessibilityWinner') {
    this[attr] = bathroom.id;
  }

  setLoser(bathroom: IBathroom, attr: 'smellWinner' | 'cleanlinessWinner' | 'viewWinner' | 'sizeWinner' | 'accessibilityWinner') {
    this[attr] = bathroom.id === this.bathroomLeft.id ? this.bathroomRight.id : this.bathroomLeft.id;
  }

  createComparison() {
    this.bathroomService.createComparison(
      this.bathroomLeft,
      this.bathroomRight,
      this.smellWinner,
      this.cleanlinessWinner,
      this.viewWinner,
      this.sizeWinner,
      this.accessibilityWinner
    ).subscribe(res => {
      this.snackBar.open(
        'Showdown submitted!',
        'Dismiss',
        {duration: 3000}
      );
      this.ngOnInit();
    });
  }

}
