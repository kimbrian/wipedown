import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {IBuilding} from "./building";
import {environment} from "../environments/environment";
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {FacultyService} from "./faculty.service";

@Injectable({
  providedIn: 'root'
})
export class BuildingService {

  constructor(private http:HttpClient, private facultyService: FacultyService) {
  }


  getBuildings(): Observable<IBuilding[]> {
    return this.http.get<IBuilding[]>(environment.apiUrl + 'api/public/buildings/', {withCredentials: true});
  }

  getBuilding(id): Observable<IBuilding> {
    return this.http.get<IBuilding>(environment.apiUrl + `api/public/buildings/${id}/`, {withCredentials: true});
  }

}
