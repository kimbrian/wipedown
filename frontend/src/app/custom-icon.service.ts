import { Injectable } from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class CustomIconService {
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
  }

  init() {
    this.matIconRegistry.addSvgIcon(
      'wdToiletPath',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        'http://s3-ap-southeast-2.amazonaws.com/wipedown-local-media/public_static_media/toilet_path_icon.svg'
      )
    );
    this.matIconRegistry.addSvgIcon(
      'wdToiletStroke',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        'http://s3-ap-southeast-2.amazonaws.com/wipedown-local-media/public_static_media/toilet_stroke_icon.svg'
      )
    );
  }
}
