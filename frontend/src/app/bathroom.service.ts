import {Injectable} from '@angular/core';
import {IBathroom} from './bathroom';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';
import {IComment} from './comment';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class BathroomService {

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {
  }

  createComparison(
    bathroomLeft: IBathroom,
    bathroomRight: IBathroom,
    smellWinner,
    cleanlinessWinner,
    viewWinner,
    sizeWinner,
    accessibilityWinner
  ): Observable<any> {
    return this.http.post<any>(environment.apiUrl + 'api/public/comparisons/', {
        bathroomLeft: bathroomLeft.id,
        bathroomRight: bathroomRight.id,
        smellWinner: smellWinner,
        cleanlinessWinner: cleanlinessWinner,
        viewWinner: viewWinner,
        sizeWinner: sizeWinner,
        accessibilityWinner: accessibilityWinner
      },
      {withCredentials: true});
  }

  get2RandomBathrooms(): Observable<IBathroom[]> {
    return this.http.get<IBathroom[]>(environment.apiUrl + 'api/public/random-bathrooms/', {withCredentials: true});
  }

  getBathrooms(): Observable<IBathroom[]> {
    return this.http.get<IBathroom[]>(environment.apiUrl + 'api/public/bathrooms/', {withCredentials: true});
  }

  getBathroom(id): Observable<IBathroom> {
    return this.http.get<IBathroom>(environment.apiUrl + `api/public/bathrooms/${id}`, {withCredentials: true});
  }

  createComment(bathroomId: number, content: string, commentType: string): Observable<IComment> {
    return this.http.post<IComment>(environment.apiUrl + 'api/public/comments/', {
      content: content,
      bathroom: bathroomId,
      commentType: commentType
    }, {withCredentials: true});
  }

  createEditRequest(editedFields) {
    return this.http.post(environment.apiUrl + 'api/public/bathroom-edits/', editedFields, {withCredentials: true}).subscribe(
      res => this.snackBar.open('Request Submitted', 'Dismiss', {
        duration: 3000
      }),
      error => this.snackBar.open('An error has occurred :(', 'Dismiss', {
        duration: 3000
      })
    );
  }

  uploadPhoto(bathroomId, file: File) {
    const formData = new FormData();
    formData.append('picture', file, file.name);
    formData.append('bathroom', bathroomId);
    return this.http.post(environment.apiUrl + 'api/public/bathroom-picture-uploads/', formData, {withCredentials: true});
  }

  getCommentsByBathroom(id): Observable<IComment[]> {
    return this.http.get<IComment[]>(environment.apiUrl + `api/public/comments/?bathroom=${id}`, {withCredentials: true});
  }

}
