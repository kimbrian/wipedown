export interface IBathroom {
  id: number;
  locationDescription: string;
  description: string;
  building: number;
  buildingName: string;
  smellElo: number;
  cleanlinessElo: number;
  viewElo: number;
  accessibilityElo: number;
  sizeElo: number;
  overallElo: number;
  pictures: string[];
  male: boolean;
  female: boolean;
  disabled: boolean;
}
