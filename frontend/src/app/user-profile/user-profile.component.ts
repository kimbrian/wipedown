import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {EditableField, EditDialogComponent} from '../edit-dialog/edit-dialog.component';
import {map} from 'rxjs/operators';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {IUser} from '../user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  public newPassword1: string;
  public newPassword2: string;
  public user: IUser;

  constructor(private authService: AuthService, private snackBar: MatSnackBar, public dialog: MatDialog) {
  }


  changePassword() {
    this.authService.changePassword(this.newPassword1, this.newPassword2).subscribe(res => {
        this.snackBar.open('Password Changed', 'Dismiss', {duration: 3000});
      },
      err => {
        const errorMsg = [].concat(...Object.values(err.error))[0];
        this.snackBar.open(errorMsg, 'Dismiss', {duration: 6000});
      });
  }

  deleteAccount() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: {
        action: 'Deactivate Account',
        confirmationQuestion: 'Are you sure you wish to deactivate your account? You will be logged out after this action.'
      }
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      console.log('The dialog was closed');
      console.log(result);
      if (result) {
        this.authService.deleteAccount().subscribe(res => this.authService.logout());
      }
    });

  }

  editDetails() {
    const dialogRef = this.dialog.open(EditDialogComponent, {
        width: '250px',
        data: [
          {
            fieldName: 'username',
            displayName: 'Username',
            fieldType: 'input',
            value: this.user.username
          },
          {
            fieldName: 'firstName',
            displayName: 'First Name',
            fieldType: 'input',
            value: this.user.firstName
          },
          {
            fieldName: 'lastName',
            displayName: 'Last Name',
            fieldType: 'input',
            value: this.user.lastName
          },
        ]
      })
    ;

    dialogRef.afterClosed().subscribe((result: EditableField[]) => {
      console.log('The dialog was closed');
      console.log(result);
      const editedFields = {};
      for (const field of result) {
        editedFields[field.fieldName] = field.value;
      }
      this.authService.editUser(editedFields).subscribe(user => this.user = user, err => {
        const errorMsg = [].concat(...Object.values(err.error))[0];
        this.snackBar.open(errorMsg, 'Dismiss', {duration: 6000});
      });
    });  }

  ngOnInit() {
    this.authService.getUser().subscribe(user => this.user = user);
  }

}
