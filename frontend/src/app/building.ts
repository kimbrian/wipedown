
export interface IBuilding {
  id: number;
  faculty: number;
  name: string;
  latitude: number;
  longitude: number;
}
