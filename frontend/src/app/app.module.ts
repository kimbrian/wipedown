import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './login/login.component';
import {AppRoutingModule} from './app-routing.module';
import {MaterialModule} from './material.module';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RegisterComponent} from './register/register.component';
import {RegisterLoginComponent} from './register-login/register-login.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BathroomListComponent} from './bathroom-list/bathroom-list.component';
import {CsrfInterceptor} from './csrf-interceptor';
import {BathroomDetailComponent} from './bathroom-detail/bathroom-detail.component';
import { BuildingsComponent } from './buildings/buildings.component';
import { BuildingListComponent } from './building-list/building-list.component';
import { BuildingDetailComponent } from './building-detail/building-detail.component';
import { InputFileComponent } from './input-file/input-file.component';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { ComparisonComponent } from './comparison/comparison.component';
import {AgmCoreModule} from '@agm/core';
import { HomeComponent } from './home/home.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ToolbarComponent,
    RegisterComponent,
    RegisterLoginComponent,
    BathroomListComponent,
    BathroomDetailComponent,
    BuildingsComponent,
    BuildingListComponent,
    BuildingDetailComponent,
    InputFileComponent,
    EditDialogComponent,
    ComparisonComponent,
    HomeComponent,
    UserProfileComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken',
    }),
    AgmCoreModule.forRoot({
      // WARNING: before deploying to prod, we must restrict
      // this key to only be used on our domain.
      apiKey: 'AIzaSyD0PdnvTFZH5grv_qTGpzXirKKR57MNNq0'
    }),
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  entryComponents: [
    EditDialogComponent,
    ConfirmDialogComponent
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
