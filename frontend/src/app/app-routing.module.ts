import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterLoginComponent} from './register-login/register-login.component';
import {BathroomListComponent} from './bathroom-list/bathroom-list.component';
import {AuthGuard} from './auth.guard';
import {BathroomDetailComponent} from './bathroom-detail/bathroom-detail.component';
import {BuildingListComponent} from "./building-list/building-list.component";
import {BuildingDetailComponent} from "./building-detail/building-detail.component";
import {ComparisonComponent} from './comparison/comparison.component';
import {HomeComponent} from "./home/home.component";
import {UserProfileComponent} from "./user-profile/user-profile.component";

const routes: Routes = [
  {path: 'login', component: RegisterLoginComponent},
  {path: 'bathrooms', component: BathroomListComponent, canActivate: [AuthGuard]},
  {path: 'comparison', component: ComparisonComponent, canActivate: [AuthGuard]},
  {path: 'bathrooms/:id', component: BathroomDetailComponent, canActivate: [AuthGuard] },
  {path: 'buildings', component: BuildingListComponent, canActivate: [AuthGuard]},
  {path: 'buildings/:id', component: BuildingDetailComponent, canActivate: [AuthGuard] },
  {path: 'home', component: HomeComponent},
  {path: 'profile', component: UserProfileComponent, canActivate: [AuthGuard]},
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
