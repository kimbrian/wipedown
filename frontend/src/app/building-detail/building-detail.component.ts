import { Component, OnInit } from '@angular/core';
import {IBuilding} from "../building";
import {ActivatedRoute} from "@angular/router";
import {BuildingService} from "../building.service";
import {IFaculty} from "../faculty";
import {FacultyService} from "../faculty.service";

@Component({
  selector: 'app-building-detail',
  templateUrl: './building-detail.component.html',
  styleUrls: ['./building-detail.component.scss']
})
export class BuildingDetailComponent implements OnInit {
  public building: IBuilding;
  public faculty: IFaculty;


  constructor(private route: ActivatedRoute, private buildingService: BuildingService, private facultyService: FacultyService) { }

  ngOnInit(): void {
    this.getBuilding();
  }

  getFaculty(): void {
    const ids = this.building.faculty;
    this.facultyService.getFaculty(ids).subscribe(f=> this.faculty=f)
  }

  getBuilding(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log('About to get building:', id);

    this.buildingService.getBuilding(id)
      .subscribe(building => {
        this.building = building; console.log('Got building:', building);
        this.getFaculty()
      });
  }
}
