import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BathroomDetailComponent } from './bathroom-detail.component';

describe('BathroomDetailComponent', () => {
  let component: BathroomDetailComponent;
  let fixture: ComponentFixture<BathroomDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BathroomDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BathroomDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
