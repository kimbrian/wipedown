import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BathroomService} from '../bathroom.service';
import {IBathroom} from '../bathroom';
import {IBuilding} from '../building';
import {BuildingService} from '../building.service';
import {IComment} from '../comment';
import {MatDialog, MatSnackBar} from '@angular/material';
import {EditableField, EditDialogComponent} from '../edit-dialog/edit-dialog.component';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-bathroom-detail',
  templateUrl: './bathroom-detail.component.html',
  styleUrls: ['./bathroom-detail.component.scss']
})
export class BathroomDetailComponent implements OnInit {
  public bathroom: IBathroom;
  // public building: IBuilding;
  public comments: IComment[] = [];
  public reports: IComment[] = [];
  public commentDraft = '';
  public reportDraft = '';

  public commentsExpanded = false;
  public reportsExpanded = false;

  constructor(private route: ActivatedRoute, private bathroomService: BathroomService, private buildingService: BuildingService, public dialog: MatDialog, public snackBar: MatSnackBar) {
  }

  public commentToggle() {
    this.commentsExpanded = !this.commentsExpanded;
  }

  public reportToggle() {
    this.reportsExpanded = !this.reportsExpanded;
  }

  public leaveComment() {
    this.bathroomService.createComment(this.bathroom.id, this.commentDraft, 'plain').subscribe(comment => {
      this.comments.unshift(comment);
      this.commentDraft = null;
    });
  }

  public leaveReport() {
    this.bathroomService.createComment(this.bathroom.id, this.reportDraft, 'alert').subscribe(comment => {
      this.reports.unshift(comment);
      this.reportDraft = null;
    });
  }

  public getColorClass(field: boolean) {
    return field ? 'accent' : 'warn';
  }

  public uploadPictures(event) {
    this.bathroomService.uploadPhoto(this.bathroom.id, event[0]).subscribe(
      resp => {
        this.snackBar.open('Image Uploaded', 'Dismiss', {
          duration: 3000
        });
        this.getBathroom();
      });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(EditDialogComponent, {
        width: '300px',
        // height: '405px',
        data: [
          {
            fieldName: 'building',
            displayName: 'Building',
            fieldType: 'select',
            optionsObservable: this.buildingService.getBuildings().pipe(
              map(buildings => buildings.map(b => {
                  return {value: b.id, display: b.name};
                })
              )
            ),
            value: this.bathroom.building
          },
          {
            fieldName: 'locationDescription',
            displayName: 'Location within building',
            fieldType: 'input',
            options: [],
            value: this.bathroom.locationDescription
          },
          {
            fieldName: 'description',
            displayName: 'Description',
            fieldType: 'text',
            options: [],
            value: this.bathroom.description
          },
          {
            fieldName: 'male',
            displayName: 'Male',
            fieldType: 'boolean',
            value: this.bathroom.male
          },
          {
            fieldName: 'female',
            displayName: 'Female',
            fieldType: 'boolean',
            value: this.bathroom.female
          },
          {
            fieldName: 'disabled',
            displayName: 'Wheelchair Accessible',
            fieldType: 'boolean',
            value: this.bathroom.disabled
          }
        ]
      })
    ;

    dialogRef.afterClosed().subscribe((result: EditableField[]) => {
      if (!result) {
        this.snackBar.open('Edit Cancelled', 'Dismiss', {duration: 3000});
        return;
      }
      console.log('The dialog was closed');
      console.log(result);
      const editedFields = {};
      for (const field of result) {
        editedFields[field.fieldName] = field.value;
      }
      editedFields['target'] = this.bathroom.id;
      this.bathroomService.createEditRequest(editedFields);
    });
  }


  ngOnInit(): void {
    this.getBathroom();
  }

  getBathroom(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.bathroomService.getCommentsByBathroom(id).subscribe(commentList => {
      this.comments = commentList.filter(c => c.commentType === 'plain');
      this.reports = commentList.filter(c => c.commentType === 'alert');
    });
    this.bathroomService.getBathroom(id)
      .subscribe(bathroom => {
        this.bathroom = bathroom;
        // this.buildingService.getBuilding(bathroom.building).subscribe(b => this.building = b);
      });
  }

}
