import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {IFaculty} from "./faculty";
import {environment} from "../environments/environment";
import {HttpClient} from '@angular/common/http';
import {IBuilding} from "./building";

@Injectable({
  providedIn: 'root'
})
export class FacultyService {

  constructor(private http:HttpClient) {
  }
    getFaculty(id): Observable<IFaculty> {
      return this.http.get<IFaculty>(environment.apiUrl + `api/public/faculties/${id}/`, {withCredentials: true});
    }

}
