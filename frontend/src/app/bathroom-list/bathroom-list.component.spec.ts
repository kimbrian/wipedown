import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BathroomListComponent } from './bathroom-list.component';

describe('BathroomListComponent', () => {
  let component: BathroomListComponent;
  let fixture: ComponentFixture<BathroomListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BathroomListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BathroomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
