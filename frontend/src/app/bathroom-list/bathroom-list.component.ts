import {Component, OnInit, ViewChild} from '@angular/core';
import {BathroomService} from '../bathroom.service';
import {IBathroom} from '../bathroom';
import {MatSort, MatTableDataSource} from '@angular/material';


@Component({
  selector: 'app-toilet-list',
  templateUrl: './bathroom-list.component.html',
  styleUrls: ['./bathroom-list.component.scss']
})
export class BathroomListComponent implements OnInit {

  public dataSource: MatTableDataSource<IBathroom>;
  public displayedColumns: string[] = [
    'id',
    'locationDescription',
    'description',
    'buildingName',
    'smellElo',
    'cleanlinessElo',
    'viewElo',
    'accessibilityElo',
    'sizeElo',
    'overallElo',
    'details'
  ];

  @ViewChild(MatSort) sort: MatSort;


  constructor(private toiletService: BathroomService) {
  }

  ngOnInit() {
    this.toiletService.getBathrooms().subscribe(toiletList => {
      this.dataSource = new MatTableDataSource(toiletList);
      this.dataSource.sort = this.sort;
    });
  }

}
