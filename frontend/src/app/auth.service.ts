import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material';
import {IUser} from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router, private snackBar: MatSnackBar
  ) {
    this.updateAuthStatus();

  }

  getUser(): Observable<IUser> {
    return this.http.get<IUser>(environment.apiUrl + 'api/auth/user', {withCredentials: true});

  }



  get isLoggedIn(): boolean {
    return !!JSON.parse(localStorage.getItem('isLoggedIn'));
  }

  get isAdmin(): boolean {
    return !!JSON.parse(localStorage.getItem('isAdmin'));
  }

  editUser(editedFields: any): Observable<IUser> {
    return this.http.patch<IUser>(environment.apiUrl + 'api/auth/user/', editedFields, {withCredentials: true});
  }

  deleteAccount() {
    return this.http.patch<IUser>(environment.apiUrl + 'api/auth/user/', {isActive: false}, {withCredentials: true});
  }

  updateAuthStatusAndRedirect() {
    this.updateAuthStatus().subscribe(
      result => this.router.navigate(['/home']),
      err => this.router.navigate(['/login'])
    );
  }

  updateAuthStatus(): Observable<any> {
    return this.http.get(environment.apiUrl + 'api/auth/user/', {withCredentials: true}).pipe(
      map((result: any) => {
        console.log('Auth status resp', result);
        localStorage.setItem('isLoggedIn', 'true');
        localStorage.setItem('isAdmin', JSON.stringify(result.isSuperuser));
        return result;
      }),
      catchError((err: any) => {
        localStorage.setItem('isLoggedIn', 'false');
        localStorage.setItem('isAdmin', 'false');
        return throwError(err);
      })
    );
  }

  changePassword(newPassword1: string, newPassword2: string): Observable<any> {
    return this.http.post(environment.apiUrl + 'api/auth/password/change/', {
      newPassword1: newPassword1,
      newPassword2: newPassword2
    }, {withCredentials: true});
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(environment.apiUrl + 'api/auth/login/', {
      'username': username,
      'password': password
    }, {withCredentials: true});
  }

  logout() {
    this.http.post(
      environment.apiUrl + 'api/auth/logout/', {}, {withCredentials: true}
    ).subscribe(resp => {
      this.updateAuthStatusAndRedirect();
    });
  }

  register(username: string, email: string, password: string, passwordRepeat: string): Observable<any> {
    return this.http.post(environment.apiUrl + 'api/auth/registration/', {
      'username': username,
      'email': email,
      'password1': password,
      'password2': passwordRepeat
    }, {withCredentials: true});
  }
}
