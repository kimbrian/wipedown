import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;


  constructor(private authService: AuthService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      passwordRepeat: ['', Validators.required]
    });

  }

  get form() {
    return this.registerForm.controls;
  }

  register() {
    this.authService.register(
      this.form.username.value,
      this.form.email.value,
      this.form.password.value,
      this.form.passwordRepeat.value
    ).subscribe(resp => {
        this.authService.updateAuthStatusAndRedirect();
      },
      err => {
        const errorMsg = [].concat(...Object.values(err.error))[0];
        this.snackBar.open(errorMsg, 'Dismiss', {duration: 6000});
      });

  }
}
