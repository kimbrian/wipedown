import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;


  constructor(private authService: AuthService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  get form() {
    return this.loginForm.controls;
  }

  login() {
    this.authService.login(this.form.username.value, this.form.password.value).subscribe(resp => {
        console.log('got response', resp);
        this.authService.updateAuthStatusAndRedirect();
      },
      err => {
        const errorMsg = [].concat(...Object.values(err.error))[0];
        this.snackBar.open(errorMsg, 'Dismiss', {duration: 6000});

      });
  }

}
