import {Component, OnInit, ViewChild} from '@angular/core';
import {BuildingService} from "../building.service";
import {MatSort, MatTableDataSource} from "@angular/material";
import {IBuilding} from "../building";

@Component({
  selector: 'app-building-list',
  templateUrl: './building-list.component.html',
  styleUrls: ['./building-list.component.scss']
})
export class BuildingListComponent implements OnInit {
  public buildings: IBuilding[];
  public dataSource: MatTableDataSource<IBuilding>;
  public displayedColumns: string[] = [
    'id',
    'faculty',
    'name',
    'details'
  ];
  @ViewChild(MatSort) sort: MatSort;

  constructor(private buildingService:BuildingService) { }

  ngOnInit() {
    this.buildingService.getBuildings().subscribe(buildings => {
      this.buildings = buildings;
      this.dataSource = new MatTableDataSource(buildings);
      this.dataSource.sort = this.sort;
    })
  }
}
