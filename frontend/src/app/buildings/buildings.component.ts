import { Component, OnInit } from '@angular/core';
import {BuildingService} from "../building.service";

@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.scss']
})
export class BuildingsComponent implements OnInit {

  constructor(private buildingService:BuildingService) { }

  ngOnInit() {
  }

}
