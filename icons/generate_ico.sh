#!/bin/bash

# square_path_icon.svg should be an svg with square canvas containing only paths
convert -background none -density 384 square_path_icon.svg -define icon:auto-resize icon.ico

