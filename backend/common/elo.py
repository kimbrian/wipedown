ELO_START_NUMBER = 1000
ELO_K_FACTOR = 50
WIN = 1
LOSS = 0


def get_probability_of_win(rating_a, rating_b):
    score_a = (1.0 / (1.0 + pow(10, (rating_a - rating_b)/400)))
    return score_a


def get_new_rating(current_rating, expected_score, actual_score, k=ELO_K_FACTOR):
    return current_rating + k * (actual_score - expected_score)

