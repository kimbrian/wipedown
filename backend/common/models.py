import datetime
import pytz

from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count
from random import randint

from common import elo
from common.elo import ELO_START_NUMBER


class RandomManager(models.Manager):
    def random_2(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, max(count - 1, 0))
        random_index_2 = randint(0, max(count - 2, 0))
        if random_index_2 >= random_index:
            random_index_2 += 1
        return [self.all()[random_index], self.all()[random_index_2]]


class EditableModel(models.Model):
    editable_fields = ()

    class Meta:
        abstract = True


class EditRequest(models.Model):
    UNDER_REVIEW = 'under review'
    APPROVED = 'approved'
    REJECTED = 'rejected'
    RequestStatusChoices = (
        (UNDER_REVIEW, UNDER_REVIEW),
        (APPROVED, APPROVED),
        (REJECTED, REJECTED)
    )
    TargetModel = EditableModel
    editable_fields = ('target',)
    admin_editable_fields = ('review_status',)
    review_status = models.CharField(max_length=32, choices=RequestStatusChoices, default=UNDER_REVIEW)
    reviewer = models.ForeignKey(User, on_delete=models.PROTECT, null=True, editable=False,
                                 related_name='%(app_label)s_%(class)s_reviewers')
    author = models.ForeignKey(User, on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_authors',
                               editable=False)
    target = models.ForeignKey(TargetModel, on_delete=models.PROTECT, null=True, related_name='requests')
    created_at = models.DateTimeField(auto_now_add=True)

    def reject(self):
        pass

    def copy_to_target(self):
        if self.target is None:
            # create the master record
            target = self.TargetModel(
                **{field_name: getattr(self, field_name) for field_name in self.TargetModel.editable_fields})
            target.save()
            self.target = target
        else:
            # update all the fields in the master record
            for field_name in self.TargetModel.editable_fields:
                setattr(self.target, field_name, getattr(self, field_name))
            self.target.save()

    class Meta:
        abstract = True


class Faculty(EditableModel):
    editable_fields = ('name',)
    name = models.TextField()


class FacultyEditRequest(EditRequest):
    TargetModel = Faculty
    target = models.ForeignKey(TargetModel, on_delete=models.PROTECT, null=True,
                               related_name='requests')
    name = models.TextField()


class Building(EditableModel):
    editable_fields = ('faculty', 'name', 'latitude', 'longitude')
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE)
    name = models.TextField()
    latitude = models.FloatField()
    longitude = models.FloatField()


class BuildingEditRequest(EditRequest):
    TargetModel = Building
    target = models.ForeignKey(TargetModel, on_delete=models.PROTECT, null=True,
                               related_name='requests')
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE)
    name = models.TextField()
    latitude = models.FloatField()
    longitude = models.FloatField()


class Bathroom(models.Model):
    objects = RandomManager()
    editable_fields = (
        "location_description",
        "description",
        "building",
        "male",
        "female",
        "disabled"
    )
    location_description = models.TextField()
    description = models.TextField()
    male = models.BooleanField()
    female = models.BooleanField()
    disabled = models.BooleanField()
    building = models.ForeignKey(Building, on_delete=models.CASCADE)
    smell_elo = models.IntegerField(editable=False, default=ELO_START_NUMBER)
    cleanliness_elo = models.IntegerField(editable=False, default=ELO_START_NUMBER)
    view_elo = models.IntegerField(editable=False, default=ELO_START_NUMBER)
    accessibility_elo = models.IntegerField(editable=False, default=ELO_START_NUMBER)
    size_elo = models.IntegerField(editable=False, default=ELO_START_NUMBER)
    overall_elo = models.IntegerField(editable=False, default=ELO_START_NUMBER)

    @property
    def pictures(self):
        return [upload.picture.url for upload in self.bathroompictureupload_set.all()]


class BathroomEditRequest(EditRequest):
    TargetModel = Bathroom
    target = models.ForeignKey(TargetModel, on_delete=models.PROTECT, null=True,
                               related_name='requests')
    location_description = models.TextField()
    description = models.TextField()
    building = models.ForeignKey(Building, on_delete=models.CASCADE)
    male = models.BooleanField()
    female = models.BooleanField()
    disabled = models.BooleanField()


class Comparison(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comparisons')
    bathroom_left = models.ForeignKey(Bathroom, on_delete=models.CASCADE, related_name='comparisons_on_left')
    bathroom_right = models.ForeignKey(Bathroom, on_delete=models.CASCADE, related_name='comparisons_on_right')
    smell_winner = models.ForeignKey(Bathroom, on_delete=models.CASCADE, related_name='comparisons_smell_won')
    cleanliness_winner = models.ForeignKey(Bathroom, on_delete=models.CASCADE,
                                           related_name='comparisons_cleanliness_won')
    view_winner = models.ForeignKey(Bathroom, on_delete=models.CASCADE, related_name='comparisons_view_won')
    size_winner = models.ForeignKey(Bathroom, on_delete=models.CASCADE, related_name='comparisons_size_won')
    accessibility_winner = models.ForeignKey(Bathroom, on_delete=models.CASCADE,
                                             related_name='comparisons_accessibility_won')
    created_at = models.DateTimeField(auto_now_add=True)

    def is_valid_bathroom(self, attribute):
        return attribute == self.bathroom_left or attribute == self.bathroom_right

    def is_valid(self):
        return all((
            self.bathroom_left != self.bathroom_right,
            self.is_valid_bathroom(self.smell_winner),
            self.is_valid_bathroom(self.cleanliness_winner),
            self.is_valid_bathroom(self.view_winner),
            self.is_valid_bathroom(self.size_winner),
            self.is_valid_bathroom(self.accessibility_winner)
        ))

    def update_bathroom_elo(self, elo_attr, winner):
        if winner != self.bathroom_left and winner != self.bathroom_right:
            raise ValueError('Winner must be either left or right bathroom')
        left_elo = getattr(self.bathroom_left, elo_attr)
        right_elo = getattr(self.bathroom_right, elo_attr)
        left_score = int(winner == self.bathroom_left)
        right_score = int(winner == self.bathroom_right)
        left_expected_score = elo.get_probability_of_win(left_elo, right_elo)
        setattr(
            self.bathroom_left,
            elo_attr,
            elo.get_new_rating(left_elo, left_expected_score, left_score)
        )
        setattr(
            self.bathroom_right,
            elo_attr,
            elo.get_new_rating(right_elo, 1 - left_expected_score, right_score)
        )

    def update_bathroom_elos(self):
        left_win_count = 0
        right_win_count = 0
        for elo_attr, winner in (
                ('smell_elo', self.smell_winner),
                ('cleanliness_elo', self.cleanliness_winner),
                ('view_elo', self.view_winner),
                ('accessibility_elo', self.accessibility_winner),
                ('size_elo', self.size_winner),
        ):
            left_win_count += int(winner == self.bathroom_left)
            right_win_count += int(winner == self.bathroom_right)
            self.update_bathroom_elo(elo_attr, winner)
        # for this to work there must always be odd number of elos
        if left_win_count > right_win_count:
            self.update_bathroom_elo('overall_elo', self.bathroom_left)
        else:
            self.update_bathroom_elo('overall_elo', self.bathroom_right)
        self.bathroom_left.save()
        self.bathroom_right.save()


class Comment(models.Model):
    PLAIN = 'plain'
    ALERT = 'alert'
    CommentTypeChoices = (
        (PLAIN, PLAIN),
        (ALERT, ALERT)
    )
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    bathroom = models.ForeignKey(Bathroom, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    comment_type = models.CharField(max_length=10, choices=CommentTypeChoices)


def get_upload_path(instance, filename):
    return 'public_pictures/{}--{}'.format(datetime.datetime.now().replace(tzinfo=pytz.utc).isoformat(), filename)


class PublicPictureUpload(models.Model):
    picture = models.ImageField(upload_to=get_upload_path)


class BathroomPictureUpload(PublicPictureUpload):
    bathroom = models.ForeignKey(Bathroom, on_delete=models.CASCADE)
