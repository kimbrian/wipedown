from django.contrib.auth.models import User
from django.test import TestCase

from common.models import Bathroom, BathroomEditRequest, Building, Comparison, Faculty, FacultyEditRequest, \
    BuildingEditRequest


class TestEditRequests(TestCase):
    fixtures = ['initial_data', 'test']

    def test_faculty_edit_request(self):
        target_faculty = Faculty.objects.get(pk=1)
        new_name = 'Edited Name'
        f = FacultyEditRequest(
            name=new_name,
            target=target_faculty,
            author=User.objects.first()
        )
        f.save()
        f.copy_to_target()
        target_faculty.refresh_from_db()
        self.assertEqual(target_faculty.name, new_name)

    def test_building_edit_request(self):
        target_building = Building.objects.get(pk=1)
        new_name = 'Edited Name'
        new_faculty = Faculty.objects.exclude(pk=target_building.faculty_id).first()
        new_latitude = 1
        new_longitude = 1
        b = BuildingEditRequest(
            target=target_building,
            faculty=new_faculty,
            latitude=new_latitude,
            longitude=new_longitude,
            name=new_name,
            author=User.objects.first()
        )
        b.save()
        b.copy_to_target()
        self.assertEqual(new_name, target_building.name)
        self.assertEqual(new_faculty, target_building.faculty)
        self.assertEqual(new_latitude, target_building.latitude)
        self.assertEqual(new_longitude, target_building.longitude)

    def test_bathroom_edit_request(self):
        target_bathroom = Bathroom.objects.get(pk=1)
        new_building = Building.objects.exclude(pk=target_bathroom.building.id).first()
        new_description = 'Edited Description'
        new_location_description = 'Edited Loc Description'
        new_male = not target_bathroom.male
        new_female = not target_bathroom.female
        new_disabled = not target_bathroom.disabled
        edit_request = BathroomEditRequest(
            author=User.objects.first(),
            target=target_bathroom,
            building=new_building,
            description=new_description,
            location_description=new_location_description,
            male=new_male,
            female=new_female,
            disabled=new_disabled
        )
        edit_request.save()
        edit_request.copy_to_target()
        target_bathroom.refresh_from_db()
        self.assertEqual(new_building, target_bathroom.building)
        self.assertEqual(new_description, target_bathroom.description)
        self.assertEqual(new_location_description, target_bathroom.location_description)
        self.assertEqual(new_male, target_bathroom.male)
        self.assertEqual(new_female, target_bathroom.female)
        self.assertEqual(new_disabled, target_bathroom.disabled)


class TestEloUpdate(TestCase):
    fixtures = ['initial_data', 'test']

    def test_elo_update(self):
        b1, b2 = Bathroom.objects.random_2()
        c = Comparison(
            bathroom_left=b1,
            bathroom_right=b2,
            smell_winner=b1,
            view_winner=b2,
            cleanliness_winner=b1,
            size_winner=b2,
            accessibility_winner=b1,
            user=User.objects.first()
        )
        c.save()
        c.update_bathroom_elos()
        b1.refresh_from_db()
        b2.refresh_from_db()
        self.assertEqual(b1.smell_elo, 1025)
        self.assertEqual(b2.view_elo, 1025)
        self.assertEqual(b1.cleanliness_elo, 1025)
        self.assertEqual(b2.size_elo, 1025)
        self.assertEqual(b1.accessibility_elo, 1025)
        self.assertEqual(b1.overall_elo, 1025)
        self.assertEqual(b2.smell_elo, 975)
        self.assertEqual(b1.view_elo, 975)
        self.assertEqual(b2.cleanliness_elo, 975)
        self.assertEqual(b1.size_elo, 975)
        self.assertEqual(b2.accessibility_elo, 975)
        self.assertEqual(b2.overall_elo, 975)
