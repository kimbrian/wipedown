import os
import toml

_CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))


class Config(object):
    def __init__(self, environment_name: str):
        secrets_file_relative_path = 'secrets/secrets.{env}.toml'.format(env=environment_name.lower())
        secrets_file = os.path.join(_CURRENT_DIR, secrets_file_relative_path)

        config_file_relative_path = 'environment.{env}.toml'.format(env=environment_name.lower())
        config_file = os.path.join(_CURRENT_DIR, config_file_relative_path)

        environment_config = toml.load(config_file)
        secret_config = toml.load(secrets_file)

        self.db = {
            'ENGINE': environment_config['db']['engine'],
            'HOST': environment_config['db']['host'],
            'PORT': environment_config['db']['port'],
            'USER': secret_config['db']['user'],
            'PASSWORD': secret_config['db']['password'],
            'NAME': secret_config['db']['name'],
        }

        self.debug = environment_config['server']['debug']
        self.allowed_hosts = environment_config['server']['allowed_hosts']
        self.secret_key = secret_config['server']['secret_key']
        self.cors_whitelist = environment_config['cors']['whitelist']
        self.cors_credentials = environment_config['cors']['credentials']
        self.file_storage_backend = environment_config['file_storage']['backend']
        self.aws_access_key_id = secret_config['aws']['aws_access_key_id']
        self.aws_secret_access_key = secret_config['aws']['aws_secret_access_key']
        self.aws_storage_bucket_name = secret_config['aws']['aws_storage_bucket_name']
        self.session_cookie_domain = environment_config['cors'].get('session_cookie_domain', None)
        self.csrf_cookie_domain = environment_config['cors'].get('csrf_cookie_domain', None)
