# Backend Instructions
## Dependencies
### Python
1. Install python 3.6 or python 3.7
2. Install pip
3. pip install virtualenv and virtualenvwrapper
4. mkvirtualenv 'wipedown-dev'
5. pip install -r requirements.txt
### Postgresql
1. Make a new db
2. Go to backend/backend/environment_config/secrets/ in the repo and make a file called secrets.local.toml. Copy the contents from secrets.example.toml into secrets.local.toml.
3. Change the default values to the values for the new db
4. No need to change the secret key for local development.
### Start Django
1. Run ```python manage.py migrate && python manage.py loaddata initial_data``` to update your db schema and load some initial data. (If you ever want to clear your db of data run ```python manage.py flush```)
2. Run ```python manage.py createsuperuser``` to create a local admin user
3. Run ```python manage.py runserver 8000``` to run the dev server on port 8000
4. Go to ```127.0.0.1:8000/api/public``` for the public api
5. Go to ```127.0.0.1:8000/api/admin``` for the admin api

