# Backend Dployment Notes
- Deployed using NGINX as reverse proxy connecting to uWSGI running in emperor mode.
- Both nginx and uWSGI are enabled on boot as systemd services
- All config files for nginx and uwsgi are in this directory
- nginx is not setup to use SSL although we definitely should do that before a real production deployment
- backend is deployed at 'http://api.wipedown.me'

