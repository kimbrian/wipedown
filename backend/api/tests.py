from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient


class ApiTestCase(TestCase):
    fixtures = ['initial_data', 'test']
    ADMIN_ENDPOINTS = ('/api/admin/bathroom-edits/',
                       '/api/admin/building-edits/',
                       '/api/admin/faculty-edits/',)

    PUBLIC_ENDPOINTS = ('/api/public/bathrooms/',
                        '/api/public/faculties/',
                        '/api/public/buildings/',
                        '/api/public/comparisons/',
                        '/api/public/bathroom-edits/',
                        '/api/public/building-edits/',
                        '/api/public/faculty-edits/',)

    def test_unauthenticated(self):
        client = APIClient()
        for endpoint in self.ADMIN_ENDPOINTS + self.PUBLIC_ENDPOINTS:
            resp = client.get(endpoint)
            self.assertEqual(resp.status_code, 403)

    def test_authenticated(self):
        client = APIClient()
        client.force_authenticate(User.objects.filter(is_superuser=False).first())
        for endpoint in self.ADMIN_ENDPOINTS:
            resp = client.get(endpoint)
            self.assertEqual(resp.status_code, 403)

        for endpoint in self.PUBLIC_ENDPOINTS:
            resp = client.get(endpoint)
            self.assertEqual(resp.status_code, 200)

    def test_authenticated_admin(self):
        client = APIClient()
        client.force_authenticate(User.objects.filter(is_superuser=True).first())
        for endpoint in self.ADMIN_ENDPOINTS + self.PUBLIC_ENDPOINTS:
            resp = client.get(endpoint)
            self.assertEqual(resp.status_code, 200)
