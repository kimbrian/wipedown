from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets

from api.serializers.comparison import ComparisonSerializer
from common.models import Comparison


class ComparisonViewSet(mixins.CreateModelMixin,
                        mixins.ListModelMixin,
                        mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):
    """
    API endpoint that allows buildings to be viewed or added.
    """
    permission_classes = (IsAuthenticated, )
    queryset = Comparison.objects.all()
    serializer_class = ComparisonSerializer

    def get_queryset(self):
        user = self.request.user
        return Comparison.objects.filter(user=user)
