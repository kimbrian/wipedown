from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from api.serializers.edit_request import (
    BathroomEditRequestSerializer, AdminBathroomEditRequestSerializer,
    BuildingEditRequestSerializer, AdminBuildingEditRequestSerializer,
    FacultyEditRequestSerializer, AdminFacultyEditRequestSerializer
)
from common.models import BathroomEditRequest, BuildingEditRequest, FacultyEditRequest


class PublicBathroomEditRequestViewSet(mixins.CreateModelMixin,
                                       mixins.RetrieveModelMixin,
                                       mixins.ListModelMixin,
                                       GenericViewSet):
    """
    API endpoint that allows bathroom edit requests to be viewed or added.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = BathroomEditRequestSerializer

    def get_queryset(self):
        return BathroomEditRequest.objects.filter(author=self.request.user)


class AdminBathroomEditRequestViewSet(ModelViewSet):
    """
    API endpoint that allows bathrooms to be viewed, added, and updated.
    """
    permission_classes = (IsAuthenticated, IsAdminUser)
    queryset = BathroomEditRequest.objects.all()
    serializer_class = AdminBathroomEditRequestSerializer


class PublicBuildingEditRequestViewSet(mixins.CreateModelMixin,
                                       mixins.RetrieveModelMixin,
                                       mixins.ListModelMixin,
                                       GenericViewSet):
    """
    API endpoint that allows bathroom edit requests to be viewed or added.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = BuildingEditRequestSerializer

    def get_queryset(self):
        return BuildingEditRequest.objects.filter(author=self.request.user)


class AdminBuildingEditRequestViewSet(ModelViewSet):
    """
    API endpoint that allows bathrooms to be viewed, added, and updated.
    """
    permission_classes = (IsAuthenticated, IsAdminUser)
    queryset = BuildingEditRequest.objects.all()
    serializer_class = AdminBuildingEditRequestSerializer


class PublicFacultyEditRequestViewSet(mixins.CreateModelMixin,
                                       mixins.RetrieveModelMixin,
                                       mixins.ListModelMixin,
                                       GenericViewSet):
    """
    API endpoint that allows bathroom edit requests to be viewed or added.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = FacultyEditRequestSerializer

    def get_queryset(self):
        return FacultyEditRequest.objects.filter(author=self.request.user)


class AdminFacultyEditRequestViewSet(ModelViewSet):
    """
    API endpoint that allows bathrooms to be viewed, added, and updated.
    """
    permission_classes = (IsAuthenticated, IsAdminUser)
    queryset = FacultyEditRequest.objects.all()
    serializer_class = AdminFacultyEditRequestSerializer
