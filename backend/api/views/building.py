from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from api.serializers.building import BuildingSerializer
from common.models import Building


class PublicBuildingViewSet(mixins.RetrieveModelMixin,
                            mixins.ListModelMixin,
                            GenericViewSet):
    """
    API endpoint that allows buildings to be viewed or added.
    """
    permission_classes = (IsAuthenticated, )
    queryset = Building.objects.all().order_by('-name')
    serializer_class = BuildingSerializer

