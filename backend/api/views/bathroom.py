from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet, ViewSet

from api.serializers.bathroom import BathroomSerializer
from common.models import Bathroom


class PublicBathroomViewSet(mixins.RetrieveModelMixin,
                            mixins.ListModelMixin,
                            GenericViewSet):
    """
    API endpoint that allows bathrooms to be viewed or added.
    """
    permission_classes = (IsAuthenticated, )
    queryset = Bathroom.objects.all().order_by('-overall_elo')
    serializer_class = BathroomSerializer


class RandomBathroomView(ViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = BathroomSerializer

    def list(self, request):
        serializer = BathroomSerializer(
            instance=Bathroom.objects.random_2(), many=True)
        return Response(serializer.data)
