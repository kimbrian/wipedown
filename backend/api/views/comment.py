from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from api.serializers.comment import CommentSerializer
from common.models import Comment


class CommentViewset(mixins.CreateModelMixin,
              mixins.RetrieveModelMixin,
              # mixins.UpdateModelMixin,
              # mixins.DestroyModelMixin,
              mixins.ListModelMixin,
              GenericViewSet):
    """
    API endpoint that allows bathroom comments to be viewed or added.
    """
    permission_classes = (IsAuthenticated,)
    queryset = Comment.objects.all().order_by('-created_at')
    serializer_class = CommentSerializer
    filterset_fields = ('bathroom', 'author')


