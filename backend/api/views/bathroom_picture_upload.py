from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from api.serializers.bathroom_picture_upload import BathroomPictureUploadSerializer
from common.models import BathroomPictureUpload


class BathroomPictureUploadViewset(mixins.CreateModelMixin,
                                   mixins.RetrieveModelMixin,
                                   # mixins.UpdateModelMixin,
                                   # mixins.DestroyModelMixin,
                                   mixins.ListModelMixin,
                                   GenericViewSet):
    """
    API endpoint that allows bathroom pictures to be viewed or added.
    """
    permission_classes = (IsAuthenticated,)
    queryset = BathroomPictureUpload.objects.all()
    serializer_class = BathroomPictureUploadSerializer
