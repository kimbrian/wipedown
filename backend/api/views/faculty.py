from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from api.serializers.faculty import FacultySerializer
from common.models import Faculty


class PublicFacultyViewSet(mixins.RetrieveModelMixin,
                           mixins.ListModelMixin,
                           GenericViewSet):
    """
    API endpoint that allows faculties to be viewed or added.
    """
    permission_classes = (IsAuthenticated,)
    queryset = Faculty.objects.all().order_by('-name')
    serializer_class = FacultySerializer
