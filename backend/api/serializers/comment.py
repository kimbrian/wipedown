from rest_framework import serializers

from common.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['author'] = self.context['request'].user
        return super(CommentSerializer, self).create(validated_data)

    class Meta:
        model = Comment
        fields = '__all__'
        read_only_fields = ('author',)
