from rest_framework import serializers

from common.models import Bathroom


class BathroomSerializer(serializers.ModelSerializer):
    pictures = serializers.ListField(child=serializers.URLField())
    building_name = serializers.CharField(source='building.name')
    class Meta:
        model = Bathroom
        fields = (
            'id',
            'building_name',
            'location_description',
            'description',
            'building',
            'smell_elo',
            'cleanliness_elo',
            'view_elo',
            'accessibility_elo',
            'size_elo',
            'overall_elo',
            'pictures',
            'male',
            'female',
            'disabled'
        )
        read_only_fields = fields
