from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from common.models import Comparison


class ComparisonSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        with transaction.atomic():
            ret: Comparison = super(ComparisonSerializer, self).create(validated_data)
            # update the bathrooms ELO here
            if not ret.is_valid():
                raise ValidationError('Invalid comparison')
            ret.update_bathroom_elos()
            return ret

    class Meta:
        model = Comparison
        fields = (
            'user',
            'bathroom_left',
            'bathroom_right',
            'smell_winner',
            'cleanliness_winner',
            'view_winner',
            'size_winner',
            'accessibility_winner',
            'created_at'
        )
        read_only_fields = ('user',)
