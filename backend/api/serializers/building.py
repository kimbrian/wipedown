from rest_framework import serializers

from common.models import Building


class BuildingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = (
            'id',
            'faculty',
            'name',
            'latitude',
            'longitude',
        )
        read_only_fields = fields