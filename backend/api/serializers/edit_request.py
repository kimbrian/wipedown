from rest_framework import serializers

from common.models import BathroomEditRequest, BuildingEditRequest, FacultyEditRequest, EditRequest


class EditRequestSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['author'] = self.context['request'].user
        return super(EditRequestSerializer, self).create(validated_data)

    class Meta:
        fields = (
            'id',
            'review_status',
            'reviewer',
            'author',
            'target',
            'created_at'
        )


class AdminEditRequestSerializer(serializers.ModelSerializer):
    def approve_or_reject(self, instance, validated_data):
        if validated_data['review_status'] == EditRequest.APPROVED:
            instance.copy_to_target()
        elif validated_data['review_status'] == EditRequest.REJECTED:
            return instance.reject()

    def create(self, validated_data):
        validated_data['author'] = self.context['request'].user
        validated_data['reviewer'] = self.context['request'].user
        instance = super(AdminEditRequestSerializer, self).create(validated_data)
        self.approve_or_reject(instance, validated_data)
        return instance

    def update(self, instance, validated_data):
        validated_data['reviewer'] = self.context['request'].user
        instance = super(AdminEditRequestSerializer, self).update(instance, validated_data)
        self.approve_or_reject(instance, validated_data)
        return instance

    class Meta:
        fields = (
            'review_status',
            'reviewer',
            'author',
            'target',
            'created_at'
        )


class BathroomEditRequestSerializer(EditRequestSerializer):
    class Meta(EditRequestSerializer.Meta):
        model = BathroomEditRequest
        fields = EditRequestSerializer.Meta.fields + (
            'location_description',
            'description',
            'building',
            'male',
            'female',
            'disabled'
        )
        read_only_fields = tuple(
            set(fields)
            - set(BathroomEditRequest.TargetModel.editable_fields)
            - set(BathroomEditRequest.editable_fields)
        )


class BuildingEditRequestSerializer(EditRequestSerializer):
    class Meta(EditRequestSerializer.Meta):
        model = BuildingEditRequest
        fields = EditRequestSerializer.Meta.fields + (
            'faculty', 'name', 'latitude', 'longitude'
        )
        read_only_fields = tuple(
            set(fields)
            - set(BuildingEditRequest.TargetModel.editable_fields)
            - set(BuildingEditRequest.editable_fields)
        )


class FacultyEditRequestSerializer(EditRequestSerializer):
    class Meta(EditRequestSerializer.Meta):
        model = FacultyEditRequest
        fields = EditRequestSerializer.Meta.fields + ('name',)
        read_only_fields = tuple(
            set(fields)
            - set(FacultyEditRequest.TargetModel.editable_fields)
            - set(FacultyEditRequest.editable_fields)
        )


class AdminBathroomEditRequestSerializer(AdminEditRequestSerializer):
    class Meta(AdminEditRequestSerializer.Meta):
        model = BathroomEditRequest
        fields = EditRequestSerializer.Meta.fields + (
            'location_description',
            'description',
            'building',
            'male',
            'female',
            'disabled'
        )
        read_only_fields = tuple(
            set(fields) -
            set(BathroomEditRequest.TargetModel.editable_fields)
            - set(BathroomEditRequest.editable_fields)
            - set(BathroomEditRequest.admin_editable_fields)
        )


class AdminBuildingEditRequestSerializer(AdminEditRequestSerializer):
    class Meta(AdminEditRequestSerializer.Meta):
        model = BuildingEditRequest
        fields = EditRequestSerializer.Meta.fields + (
            'faculty', 'name', 'latitude', 'longitude'
        )
        read_only_fields = tuple(
            set(fields)
            - set(BuildingEditRequest.TargetModel.editable_fields)
            - set(BuildingEditRequest.editable_fields)
            - set(BuildingEditRequest.admin_editable_fields)
        )


class AdminFacultyEditRequestSerializer(AdminEditRequestSerializer):
    class Meta(AdminEditRequestSerializer.Meta):
        model = FacultyEditRequest
        fields = EditRequestSerializer.Meta.fields + ('name',)
        read_only_fields = tuple(
            set(fields)
            - set(FacultyEditRequest.TargetModel.editable_fields)
            - set(FacultyEditRequest.editable_fields)
            - set(FacultyEditRequest.admin_editable_fields)
        )