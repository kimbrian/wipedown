from rest_framework import serializers

from common.models import Faculty


class FacultySerializer(serializers.ModelSerializer):
    class Meta:
        model = Faculty
        fields = (
            'id',
            'name',
        )
        read_only_fields = fields