from rest_framework import serializers

from common.models import BathroomPictureUpload


class BathroomPictureUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = BathroomPictureUpload
        fields = '__all__'
