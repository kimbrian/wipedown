from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers
from api.views import bathroom
from api.views import comment
from api.views import comparison
from api.views import building
from api.views import faculty
from api.views import edit_request
from api.views import bathroom_picture_upload

router = routers.DefaultRouter()
router.register(r'bathrooms', bathroom.PublicBathroomViewSet)
router.register(r'comments', comment.CommentViewset)
router.register(r'bathroom-picture-uploads', bathroom_picture_upload.BathroomPictureUploadViewset)
router.register(r'random-bathrooms', bathroom.RandomBathroomView, base_name='random-bathrooms')
router.register(r'comparisons', comparison.ComparisonViewSet)
router.register(r'buildings', building.PublicBuildingViewSet)
router.register(r'faculties', faculty.PublicFacultyViewSet)
router.register(r'bathroom-edits', edit_request.PublicBathroomEditRequestViewSet, base_name='bathroom-edits')
router.register(r'building-edits', edit_request.PublicBuildingEditRequestViewSet, base_name='building-edits')
router.register(r'faculty-edits', edit_request.PublicFacultyEditRequestViewSet, base_name='faculty-edits')

admin_router = routers.DefaultRouter()
admin_router.register(r'bathroom-edits', edit_request.AdminBathroomEditRequestViewSet, base_name='admin-bathroom-edits')
admin_router.register(r'building-edits', edit_request.AdminBuildingEditRequestViewSet, base_name='admin-building-edits')
admin_router.register(r'faculty-edits', edit_request.AdminFacultyEditRequestViewSet, base_name='admin-faculty-edits')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^public/', include(router.urls)),
    url(r'^admin/', include(admin_router.urls)),
    path('auth/', include('rest_auth.urls')),
    url(r'^auth/registration/', include('rest_auth.registration.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]